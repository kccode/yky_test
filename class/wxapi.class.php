<?php
    /********************************************************
	 *
	 *		@author Kyler You <QQ:2444756311>
	 *
	 *		@link http://mp.weixin.qq.com/wiki/home/index.html
	 *
	 *		@version 2.0.1
	 *
	 *		@uses $wxApi = new WxApi();
	 *
	 *		@package 微信API接口 陆续会继续进行更新
	 *
	 ********************************************************/
	class WxApi {
	
		const appId 	= "";
		const appSecret = "";
		
		public function __construct(){
		
		}
		
		/****************************************************
		 *
		 *	微信提交API方法，返回微信指定JSON
		 *
		 ****************************************************/
		
		public function wxHttpsRequest($url,$data = null){
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
			if (!empty($data)){
				curl_setopt($curl, CURLOPT_POST, 1);
				curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			}
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			$output = curl_exec($curl);
			curl_close($curl);
			return $output;
		}

		/****************************************************
		 *
		 *	微信获取AccessToken 返回指定微信公众号的at信息
		 *
		 ****************************************************/
		
		public function wxAccessToken($appId = NULL , $appSecret = NULL){
			$appId 			= is_null($appId) ? self::appId : $appId;
			$appSecret 		= is_null($appSecret) ? self::appSecret : $appSecret;
			//echo $appId,$appSecret;
			$url 			= "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=".$appId."&secret=".$appSecret;
			$result 		= $this->wxHttpsRequest($url);
			//print_r($result);
			$jsoninfo 		= json_decode($result, true);
			$access_token 	= $jsoninfo["access_token"];
			return $access_token;
		}
		
		/****************************************************
		 *
		 *	微信通过OPENID获取用户信息，返回数组
		 *
		 ****************************************************/
		 
		public function wxGetUser($openId){
			$wxAccessToken 	= $this->wxAccessToken();
			$url 			= "https://api.weixin.qq.com/cgi-bin/user/info?access_token=".$wxAccessToken."&openid=".$openId."&lang=zh_CN";
			$result 		= $this->wxHttpsRequest($url);
			$jsoninfo 		= json_decode($result, true);
			return $jsoninfo;
		}

		/****************************************************
		 *
		 *	微信通过指定模板信息发送给指定用户，发送完成后返回指定JSON数据
		 *
		 ****************************************************/
		
		public function wxSendTemplate($jsonData){
			$wxAccessToken 	= $this->wxAccessToken();
			$url			= "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=".$wxAccessToken;
			$result 		= $this->wxHttpsRequest($url,$jsonData);
			return $result;
		}

		/****************************************************
		 *
		 *	微信设置OAUTH跳转URL，返回字符串信息 - SCOPE = snsapi_base //验证时不返回确认页面，只能获取OPENID
		 *
		 ****************************************************/
		
		public function wxOauthBase($redirectUrl,$state = "",$appId = NULL){
			$appId 			= is_null($appId) ? self::appId : $appId;
			$url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=".$appId."&redirect_uri=".$redirectUrl."&response_type=code&scope=snsapi_base&state=".$state."#wechat_redirect";
			return $url;
		}

		/****************************************************
		 *
		 *	微信设置OAUTH跳转URL，返回字符串信息 - SCOPE = snsapi_userinfo //获取用户完整信息
		 *
		 ****************************************************/
		
		public function wxOauthUserinfo($redirectUrl,$state = "",$appId = NULL){
			$appId 			= is_null($appId) ? self::appId : $appId;
			$url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=".$appId."&redirect_uri=".$redirectUrl."&response_type=code&scope=snsapi_userinfo&state=".$state."#wechat_redirect";
			return $url;
		}

		/****************************************************
		 *
		 *	微信OAUTH跳转指定URL
		 *
		 ****************************************************/
		
		public function wxHeader($url){
			header("location:".$url);
		}

		/****************************************************
		 *
		 *	微信通过OAUTH返回页面中获取AT信息
		 *
		 ****************************************************/
		
		public function wxOauthAccessToken($code,$appId = NULL , $appSecret = NULL){
			$appId 			= is_null($appId) ? self::appId : $appId;
			$appSecret 		= is_null($appSecret) ? self::appSecret : $appSecret;
			$url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=".$appId."&secret=".$appSecret."&code=".$code."&grant_type=authorization_code";
			$result 		= $this->wxHttpsRequest($url);
			//print_r($result);
			$jsoninfo 		= json_decode($result, true);
			//$access_token 	= $jsoninfo["access_token"];
			return $jsoninfo;			
		}

		/****************************************************
		 *
		 *	微信通过OAUTH的Access_Token的信息获取当前用户信息 // 只执行在snsapi_userinfo模式运行
		 *
		 ****************************************************/
		
		public function wxOauthUser($OauthAT,$openId){
			$url 			= "https://api.weixin.qq.com/sns/userinfo?access_token=".$OauthAT."&openid=".$openId."&lang=zh_CN";
			$result 		= $this->wxHttpsRequest($url);
			$jsoninfo 		= json_decode($result, true);
			return $jsoninfo;			
		}
		
	}